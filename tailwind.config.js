/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');
module.exports = {
  content:  ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      maxWidth: {
        '360': '360px'
      },
      fontSize: {
        '11': '11px',
        '13': '13px',
      },
      backgroundColor: {
        'bage': '#2F783E'
      },
      boxShadow: {
        '3xl': '0px 1px 23px 0px rgba(119, 119, 119, 0.49)',
        '4xl': '0px 1px 23px 0px rgba(186, 186, 186, 0.49)',
      },
      colors: {
        purple: {
          primary: "#461D75",
        },
        black: {
          primary: "#383838",
          secondary: '#202020',
          light: "#D9D9D9",
        },
        red: {
          primary: "#FF4A4F",
          secondary: "#A94140",
        }
      },
      textUnderlineOffset: {
        5: '5px',
      },
      fontFamily: {
        'nunito' : ['"Nunito"', ...defaultTheme.fontFamily.sans]
      }
    },
  },
  plugins: [],
}

